package ru.kharlamova.tm.enumerated;

import ru.kharlamova.tm.comparator.ComparatorByCreated;
import ru.kharlamova.tm.comparator.ComparatorByDateStart;
import ru.kharlamova.tm.comparator.ComparatorByName;
import ru.kharlamova.tm.comparator.ComparatorByStatus;

import java.util.Comparator;

public enum Sort {

    CREATED("Sort by created", ComparatorByCreated.getInstance()),
    DATE_START("Sort by start date", ComparatorByDateStart.getInstance()),
    NAME("Sort by name", ComparatorByName.getInstance()),
    STATUS("Sort by status", ComparatorByStatus.getInstance());

    private String displayName;

    private final Comparator comparator;

    Sort(String displayName, Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public Comparator getComparator() {
        return comparator;
    }

    public String getDisplayName() {
        return displayName;
    }

}
