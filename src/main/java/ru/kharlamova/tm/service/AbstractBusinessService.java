package ru.kharlamova.tm.service;

import ru.kharlamova.tm.api.IBusinessRepository;
import ru.kharlamova.tm.api.IBusinessService;
import ru.kharlamova.tm.enumerated.Status;
import ru.kharlamova.tm.exception.empty.EmptyIdException;
import ru.kharlamova.tm.exception.empty.EmptyNameException;
import ru.kharlamova.tm.exception.empty.EmptyUserIdException;
import ru.kharlamova.tm.exception.entity.ObjectNotFoundException;
import ru.kharlamova.tm.exception.system.IndexIncorrectException;
import ru.kharlamova.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public abstract class AbstractBusinessService<E extends AbstractBusinessEntity> extends AbstractService<E> implements IBusinessService<E> {

    private IBusinessRepository<E> repository;

    public AbstractBusinessService(IBusinessRepository<E> repository) {
        super(repository);
        this.repository = repository;
    }

    public List<E> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return repository.findAll(userId);
    }

    public List<E> findAll(final String userId, final Comparator<E> comparator) {
        if (!Optional.ofNullable(comparator).isPresent()) return null;
        return repository.findAll(userId, comparator);
    }

    public E add(final String userId, final E entity) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (!Optional.ofNullable(entity).isPresent()) throw new ObjectNotFoundException();
        return repository.add(userId, entity);
    }

    public Optional<E> findById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(userId, id);
    }

    public Optional<E> findByIndex(final String userId, Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        return repository.findByIndex(userId, index);
    }

    public Optional<E> findByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return repository.findByName(userId, name);
    }

    public void clear(final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        repository.clear(userId);
    }

    public Optional<E> updateByIndex(final String userId, final Integer index, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Optional<E> entity = findByIndex(userId, index);
        entity.ifPresent(e -> {
            e.setName(name);
            e.setDescription(description);
        });
        entity.orElseThrow(ObjectNotFoundException::new);
        return entity;
    }

    public Optional<E> updateById(final String userId, final String id, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Optional<E> entity = findById(userId, id);
        if (entity == null) return null;
        entity.ifPresent(e -> {
            e.setId(id);
            e.setName(name);
            e.setDescription(description);
        });
        entity.orElseThrow(ObjectNotFoundException::new);
        return entity;
    }

    public Optional<E> startByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Optional<E> entity = findByIndex(userId, index);
        entity.ifPresent(e -> e.setStatus(Status.IN_PROGRESS));
        entity.orElseThrow(ObjectNotFoundException::new);
        return entity;
    }

    public Optional<E> startById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Optional<E> entity = findById(userId, id);
        entity.ifPresent(e -> e.setStatus(Status.IN_PROGRESS));
        entity.orElseThrow(ObjectNotFoundException::new);
        return entity;
    }

    public Optional<E> startByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Optional<E> entity = findByName(userId, name);
        entity.ifPresent(e -> e.setStatus(Status.IN_PROGRESS));
        entity.orElseThrow(ObjectNotFoundException::new);
        return entity;
    }

    public Optional<E> finishByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Optional<E> entity = findByIndex(userId, index);
        entity.ifPresent(e -> e.setStatus(Status.COMPLETE));
        entity.orElseThrow(ObjectNotFoundException::new);
        return entity;
    }

    public Optional<E> finishById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Optional<E> entity = findById(userId, id);
        entity.ifPresent(e -> e.setStatus(Status.COMPLETE));
        entity.orElseThrow(ObjectNotFoundException::new);
        return entity;
    }

    public Optional<E> finishByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Optional<E> entity = findByName(userId, name);
        entity.ifPresent(e -> e.setStatus(Status.COMPLETE));
        entity.orElseThrow(ObjectNotFoundException::new);
        return entity;
    }

    public Optional<E> changeStatusByIndex(final String userId, final Integer index, final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Optional<E> entity = findByIndex(userId, index);
        entity.ifPresent(e -> e.setStatus(status));
        entity.orElseThrow(ObjectNotFoundException::new);
        return entity;
    }

    public Optional<E> changeStatusById(final String userId, final String id, final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Optional<E> entity = findById(userId, id);
        entity.ifPresent(e -> e.setStatus(status));
        entity.orElseThrow(ObjectNotFoundException::new);
        return entity;
    }

    public Optional<E> changeStatusByName(final String userId, final String name, final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Optional<E> entity = findByName(userId, name);
        entity.ifPresent(e -> e.setStatus(status));
        entity.orElseThrow(ObjectNotFoundException::new);
        return entity;
    }

    public void remove(final String userId, final E entity) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (!Optional.ofNullable(entity).isPresent()) throw new ObjectNotFoundException();
        repository.remove(userId, entity);
    }

    public E removeById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.removeById(userId, id);
    }

    public E removeByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        return repository.removeByIndex(userId, index);
    }

    public E removeByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return repository.removeByName(userId, name);
    }

}
