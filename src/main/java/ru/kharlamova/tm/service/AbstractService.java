package ru.kharlamova.tm.service;

import ru.kharlamova.tm.api.IRepository;
import ru.kharlamova.tm.api.IService;
import ru.kharlamova.tm.exception.empty.EmptyIdException;
import ru.kharlamova.tm.exception.entity.ObjectNotFoundException;
import ru.kharlamova.tm.model.AbstractEntity;

import java.util.List;
import java.util.Optional;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    private IRepository<E> repository;

    public AbstractService(IRepository<E> repository) {
        this.repository = repository;
    }

    public List<E> findAll() {
        return repository.findAll();
    }

    public E add(final E entity) {
        if (!Optional.ofNullable(entity).isPresent()) throw new ObjectNotFoundException();
        return  repository.add(entity);
    }

    public Optional<E> findById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(id);
    }

    public void clear() {
        repository.clear();
    }

    public E removeById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.removeById(id);
    }

    public void remove(final E entity) {
        if (!Optional.ofNullable(entity).isPresent()) throw new ObjectNotFoundException();
        repository.remove(entity);
    }

}
