package ru.kharlamova.tm.service;

import ru.kharlamova.tm.api.repository.IUserRepository;
import ru.kharlamova.tm.api.service.IUserService;
import ru.kharlamova.tm.enumerated.Role;
import ru.kharlamova.tm.exception.authorization.LoginExistException;
import ru.kharlamova.tm.exception.empty.*;
import ru.kharlamova.tm.model.User;
import ru.kharlamova.tm.util.HashUtil;

import java.util.Optional;

public class UserService extends AbstractService<User> implements IUserService {

    private final IUserRepository userRepository;

    public UserService(IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Override
    public User create(String login, String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        userRepository.add(user);
        return user;
    }

    @Override
    public User create(String login, String password, String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        final User user = create(login, password);
        if (user == null) return null;
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(String login, String password, Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        final User user = create(login, password);
        if (user == null) return null;
        user.setRole(role);
        return user;
    }

    @Override
    public Optional<User> setPassword(final String userId, final String password) {
        if (userId == null || userId.isEmpty()) throw new EmptyPasswordException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final Optional<User> user = findById(userId);
        if (user == null) return null;
        final String hash = HashUtil.salt(password);
        user.ifPresent(u -> u.setPasswordHash(hash));
        return user;
    }

    @Override
    public Optional<User> findByLogin(String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    public Optional<User> findByEmail(final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        return userRepository.findByEmail(email);
    }

    @Override
    public User removeByLogin(String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.removeByLogin(login);
    }

    @Override
    public Optional<User> updateUser(String userId, String firstName, String lastName, String middleName) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final Optional<User> user = findById(userId);
        user.ifPresent(u -> {
            u.setFirstName(firstName);
            u.setLastName(lastName);
            u.setMiddleName(middleName);
        });
        return user;
    }

    @Override
    public boolean isLoginExist(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExist(final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        return findByEmail(email) != null;
    }

}