package ru.kharlamova.tm.api.repository;
import ru.kharlamova.tm.api.IBusinessRepository;
import ru.kharlamova.tm.model.Task;
import java.util.List;
import java.util.Optional;

public interface ITaskRepository extends IBusinessRepository<Task> {

    List<Task> findAllByProjectId(String userId, String projectId);

    void removeAllByProjectId(String userId, String projectId);

    Optional<Task> bindTaskByProject(String userId, String taskId, String projectId);

    Optional<Task> unbindTaskFromProject(String userId, String projectId);

}
