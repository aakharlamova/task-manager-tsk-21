package ru.kharlamova.tm.api.repository;

import ru.kharlamova.tm.api.IRepository;
import ru.kharlamova.tm.model.User;

import java.util.Optional;

public interface IUserRepository extends IRepository<User> {

    Optional<User> findByLogin(String login);

    Optional<User> findByEmail(String email);

    User removeByLogin(String login);

}