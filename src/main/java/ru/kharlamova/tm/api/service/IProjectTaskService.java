package ru.kharlamova.tm.api.service;

import ru.kharlamova.tm.model.Project;
import ru.kharlamova.tm.model.Task;

import java.util.List;
import java.util.Optional;

public interface IProjectTaskService {

    List<Task> findAllTaskByProjectId(String userId, String projectId);

    Optional<Task> bindTaskByProject(String userId, String taskId, String projectId);

    Optional<Task> unbindTaskFromProject(String userId, String taskId);

    Project removeProjectById(String userId, String projectId);

}
