package ru.kharlamova.tm.api.service;


public interface ServiceLocator {

    ITaskService getTaskService();

    IProjectService getProjectService();

    ICommandService getCommandService();

    IProjectTaskService getProjectTaskService();

    IAuthService getAuthService();

    IUserService getUserService();

}
