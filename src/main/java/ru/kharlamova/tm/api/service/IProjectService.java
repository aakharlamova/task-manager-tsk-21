package ru.kharlamova.tm.api.service;

import ru.kharlamova.tm.api.IBusinessService;
import ru.kharlamova.tm.model.Project;

public interface IProjectService extends IBusinessService<Project> {

    Project add(String userId, String name, String description);

}
