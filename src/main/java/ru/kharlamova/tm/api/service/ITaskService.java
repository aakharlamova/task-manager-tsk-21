package ru.kharlamova.tm.api.service;

import ru.kharlamova.tm.api.IBusinessService;
import ru.kharlamova.tm.model.Task;

public interface ITaskService extends IBusinessService<Task> {

    Task add(String userId, String name, String description);

}
