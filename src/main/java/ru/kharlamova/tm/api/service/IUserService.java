package ru.kharlamova.tm.api.service;

import ru.kharlamova.tm.enumerated.Role;
import ru.kharlamova.tm.model.User;

import java.util.Optional;

public interface IUserService {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    Optional<User> setPassword(String userId, String password);

    Optional<User> findById(String id);

    Optional<User> findByLogin(String login);

    Optional<User> findByEmail(String email);

    User removeByLogin(String login);

    boolean isLoginExist(String login);

    boolean isEmailExist(String email);

    Optional<User> updateUser(String userId, String firstName, String lastName, String middleName);

}
