package ru.kharlamova.tm.repository;

import ru.kharlamova.tm.api.IRepository;
import ru.kharlamova.tm.model.AbstractEntity;

import java.util.*;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> entities = new ArrayList<>();

    @Override
    public List<E> findAll() {
        final List<E> result = new ArrayList<>();
        for (final E entity : entities) {
            if (entity.equals(entity.getId())) result.add(entity);
        }
        return entities;
    }

    @Override
    public E add(E entity) {
        entities.add(entity);
        return entity;
    }

    @Override
    public Optional<E> findById(final String id) {
        return entities.stream()
                .filter(entity -> id.equals(entity.getId()))
                .findFirst();
    }

    @Override
    public void clear() {
        entities.clear();
    }

    @Override
    public E removeById(final String id) {
        final Optional<E> entity = findById(id);
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

    @Override
    public void remove(final E entity) {
        entities.remove(entity);
    }

}
