package ru.kharlamova.tm.repository;

import ru.kharlamova.tm.api.repository.ITaskRepository;
import ru.kharlamova.tm.model.Task;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class TaskRepository extends AbstractBusinessRepository<Task> implements ITaskRepository {

    public static Predicate<Task> predicateByProjectId(final String projectId) {
        return s -> projectId.equals(s.getProjectId());
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, String projectId) {
        return entities.stream()
                .filter(predicateByUserId(userId))
                .filter(predicateByProjectId(projectId))
                .collect(Collectors.toList());
    }

    @Override
    public void removeAllByProjectId(final String userId, String projectId) {
        findAllByProjectId(userId, projectId).forEach(task -> entities.remove(task));
    }

    @Override
    public Optional<Task> bindTaskByProject(final String userId, String taskId, String projectId) {
        final Optional<Task> task = findById(userId, taskId);
        if (!task.isPresent()) return Optional.empty();
        task.get().setProjectId(projectId);
        return task;
    }

    @Override
    public Optional<Task> unbindTaskFromProject(final String userId, String taskId) {
        final Optional<Task> task = findById(userId, taskId);
        if (!task.isPresent()) return Optional.empty();
        task.get().setProjectId(null);
        return task;
    }

}
