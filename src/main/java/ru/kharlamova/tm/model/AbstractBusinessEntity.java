package ru.kharlamova.tm.model;

import ru.kharlamova.tm.enumerated.Status;

import java.util.Date;

public abstract class AbstractBusinessEntity extends AbstractEntity {

    protected String name = "";

    protected String description = "";

    protected Status status = Status.NOT_STARTED;

    protected String userId;

    protected Date dateStart;

    protected Date created = new Date();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String toString() {
        return id + ": " + name + " - " + description;
    }
    
}
