package ru.kharlamova.tm.model;

import ru.kharlamova.tm.api.entity.IWBS;

public class Task extends AbstractBusinessEntity implements IWBS {

    private String projectId;

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public Task() {
    }

    public Task(String name) {
        this.name = name;
    }

}
