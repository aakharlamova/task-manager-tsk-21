package ru.kharlamova.tm.command.task;

import ru.kharlamova.tm.command.AbstractTaskCommand;
import ru.kharlamova.tm.model.Task;
import ru.kharlamova.tm.util.TerminalUtil;

import java.util.List;

public class TaskByProjectIdShowAllCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-view-by-project-id";
    }

    @Override
    public String description() {
        return "Show tasks by project id.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW TASK]");
        System.out.println("[ENTER ID:]");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks = serviceLocator.getProjectTaskService().findAllTaskByProjectId(userId, projectId);
        int index = 1;
        for (final Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

}
