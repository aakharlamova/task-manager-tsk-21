package ru.kharlamova.tm.command.task;

import ru.kharlamova.tm.command.AbstractTaskCommand;
import ru.kharlamova.tm.exception.entity.TaskNotFoundException;
import ru.kharlamova.tm.model.Task;
import ru.kharlamova.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskByNameRemoveCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-remove-by-name";
    }

    @Override
    public String description() {
        return "Delete a task by name.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[REMOVE TASK]");
        System.out.println("[ENTER NAME:]");
        final String name = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().removeByName(userId, name);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
    }

}
