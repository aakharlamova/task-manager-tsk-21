package ru.kharlamova.tm.command.authorization;

import ru.kharlamova.tm.command.AbstractCommand;
import ru.kharlamova.tm.model.User;
import ru.kharlamova.tm.util.TerminalUtil;

import java.util.Optional;

public class UserUpdateProfileCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "update-profile";
    }

    @Override
    public String description() {
        return "Update info about your profile.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UPDATE PROFILE]");
        System.out.println("[ENTER FIRST NAME:]");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("[ENTER LAST NAME:]");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("[ENTER MIDDLE NAME:]");
        final String middleName = TerminalUtil.nextLine();
        final Optional<User> user = serviceLocator.getUserService().updateUser(userId, firstName, lastName, middleName);
        System.out.println("[OK]");
    }

}
