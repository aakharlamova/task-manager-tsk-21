package ru.kharlamova.tm.command.authorization;

import ru.kharlamova.tm.command.AbstractCommand;
import ru.kharlamova.tm.model.User;
import ru.kharlamova.tm.util.TerminalUtil;

import java.util.Optional;

public class UserChangePasswordCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "change-password";
    }

    @Override
    public String description() {
        return "Change password for current user";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("[ENTER NEW PASSWORD:]");
        final String newPassword  = TerminalUtil.nextLine();
        final Optional<User> user = serviceLocator.getUserService().setPassword(userId, newPassword);
        System.out.println("[OK]");
    }

}
