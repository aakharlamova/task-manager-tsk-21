package ru.kharlamova.tm.command.system;

import ru.kharlamova.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.Optional;

public class ArgumentsListCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "arguments";
    }

    @Override
    public String description() {
        return "Show program arguments.";
    }

    @Override
    public void execute() {
        final Collection<AbstractCommand> arguments = serviceLocator.getCommandService().getArguments();
        for (final AbstractCommand argument : arguments) {
            final String arg = argument.arg();
            if (!Optional.ofNullable(arg).isPresent()) continue;
            System.out.println(arg);
        }
    }

}
