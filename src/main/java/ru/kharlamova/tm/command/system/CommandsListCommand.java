package ru.kharlamova.tm.command.system;

import ru.kharlamova.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.Optional;

public class CommandsListCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "commands";
    }

    @Override
    public String description() {
        return "Show program commands.";
    }

    @Override
    public void execute() {
        final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getCommands();
        for (final AbstractCommand command : commands) {
            final String name = command.name();
            if (!Optional.ofNullable(name).isPresent()) continue;
            System.out.println(name);
        }
    }

}
