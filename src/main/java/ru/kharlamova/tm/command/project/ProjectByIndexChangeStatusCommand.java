package ru.kharlamova.tm.command.project;

import ru.kharlamova.tm.command.AbstractProjectCommand;
import ru.kharlamova.tm.enumerated.Status;
import ru.kharlamova.tm.exception.entity.ProjectNotFoundException;
import ru.kharlamova.tm.model.Project;
import ru.kharlamova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Optional;

public class ProjectByIndexChangeStatusCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-change-status-by-index";
    }

    @Override
    public String description() {
        return "Change project status by project index.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CHANGE PROJECT]");
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("[ENTER STATUS:]");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Optional<Project> project = serviceLocator.getProjectService().findByIndex(userId, index);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        final Optional<Project> projectStatusUpdate = serviceLocator.getProjectService().changeStatusByIndex(userId, index, status);
        Optional.ofNullable(projectStatusUpdate).orElseThrow(ProjectNotFoundException::new);
    }

}
